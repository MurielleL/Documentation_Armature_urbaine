---
title: "Identification d'une armature urbaine à l'échelle régionale"
author: "Karine Carpentier Haugmard - Bruno Dardaillon - Anne-Marie Dumas - Murielle Lethrosne - André Pages"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
link-citations: yes
output:
  bookdown::gitbook: 
    includes:

    config:
      toc:
        before: |
          <li><p style="text-align:center;"><img src="https://mtes-mct.github.io/parcours-r/assets/images/logo_marianne_fond_blanc.png" style="width:100px;"/></p></li>
          <li><a href="./">L'Armature Urbaine</a></li>
        after: |
          <li><a href="https://gitlab.com/MurielleL/Documentation_Armature_urbaine">
          See the source</a></li>
      edit: https://gitlab.com/MurielleL/Documentation_Armature_urbaine/edit/master/%s
    download: ["pdf", "epub"]
bookdown::epub_book:
  stylesheet: style.css
bookdown::pdf_book:
  latex_engine: pdflatex
  citation_package: natbib
---

<style> 
  body {text-align: justify;}
</style>


```{r setup, eval=T,echo=F,message=F,warning=F}
rm(list=ls())
knitr::opts_chunk$set(cache=T,echo=T,comment="",message=F,warning = F)

```


```{r install pkg,include=FALSE,cache=TRUE}

install.packages("readODS",repos = "http://cran.us.r-project.org")
install.packages("sf",repos = "http://cran.us.r-project.org",type="source")
remotes::install_github("MaelTheuliere/COGiter")
```


```{r eval=TRUE}
install.packages('udunits2', type = 'source', repo = 'cran.rstudio.com', configure.args = "--with-udunits2-lib=/usr/local/Cellar/udunits/2.2.20/lib/")
#devtools::install_github("r-spatial/sf")
# or the development version
# devtools::install_github("rstudio/bookdown")
```

#Présentation du projet

Le travail inter-dreal actuel s’appuie sur l’expérience réalisée en Nouvelle-Aquitaine. 

L’objectif de la démarche initiale était d’identifier l’armature urbaine de la Nouvelle-Aquitaine, ce qui correspond à identifier la hiérarchie des villes et leurs aires d’influence au sein de ce territoire régional.
Pour comprendre cette structuration et hiérarchiser les villes, il a été nécessaire d’évaluer la nature et l’ampleur du rôle de chacune d’elle. Pour ce faire, les auteurs ont cherché à mesurer le niveau de fonctions centrales de chaque ville ainsi que leur degré de polarisation. Les indicateurs mesurant ce niveau de centralité s’appuient sur deux fonds théoriques de référence en la matière, la théorie des lieux centraux de W. Christaller (1933) et la loi rang-taille de G.K. Zipf (1949) [^1] . Ces deux théories reposent sur des critères de population, d’équipements et de services. Les auteurs ont renouvelé l’approche en faisant appel à d’autres variables pour tenir compte des spécificités régionales et pour proposer une lecture plus fine du territoire. Ils ont donc déterminé des indicateurs dans trois domaines :
- le domaine socio-économique regroupant 12 indicateurs (28 % des indicateurs) dans lequel on retrouve l’indicateur de population des théories de référence, complété d’indicateurs sur le logement et l’emploi. Le recours à ces indicateurs supplémentaires est justifié par le fait que plus les villes concentrent de population, d’emplois et de logements, plus leur pouvoir de centralité et d’attraction est important.
- le domaine des équipements et services, regroupant 28 indicateurs (67 % des indicateurs), avec des indicateurs de services publics et généraux, des indicateurs d’équipements et services dans le domaine de l’enseignement, de la santé et du social, de la culture, des loisirs et du sport. Les équipements et services ont un poids volontairement important, car les auteurs ont estimé, au regard des théories de référence, que ceux-ci étaient représentatifs des pouvoirs dont jouissent les villes, en particulier celui d’attractivité. De plus, les auteurs ont constaté que la localisation et la répartition du secteur tertiaire était révélateur du niveau de centralité des villes. En d’autres termes, plus les villes concentrent des équipements et services en quantité, diversifiés et certains à recours rare, plus leur niveau de centralité est élevé.
- le domaine de la mobilité et le positionnement avec deux indicateurs (5 % des indicateurs), un sur la présence d’une gare et un sur l’accessibilité à un panier de 29 équipements. Ils traduisent le potentiel d’accessibilité d’une ville. Les auteurs ont ajouté ces indicateurs, car les informations qu’ils donnent sur l’accessibilité, sont susceptibles d’intervenir dans l’attractivité.

Au total, 42 indicateurs ont été sélectionnés pour partitionner l’ensemble des communes de Nouvelle-Aquitaine à l’aide d’une classification ascendante hiérarchique (CAH). Les auteurs ont obtenu un classement des centralités en différents groupes. Puis la polarisation des villes a été étudiée au regard des flux domicile-travail et domicile-étude. Ce n’est qu’après ces différentes étapes qu’il a été possible d’obtenir, de façon objective, la hiérarchie des villes jouant un rôle à l’échelle de la région Nouvelle-Aquitaine. De fait, de nombreuses centralités, qui jouent un rôle à une échelle plus locale n’ont pas été retenues, car n’étant pas suffisamment haut dans la hiérarchie pour jouer un rôle à l’échelle étudiée. 

[^1]: La théorie des lieux centraux s’attache à expliquer l’espacement et les inégalités de taille des villes en les mettant en relation avec leurs fonctions économiques. Cette théorie repose sur le concept de centralité : la position centrale d’une ville facilite son accessibilité et par conséquent, favorise les échanges de biens et de services. Les biens et services quotidiens se trouvent dans les petites villes proches de la population tandis que les biens et services plus spécialisés se trouvent dans les villes plus grandes. La notion de centralité repose sur la distance que la clientèle est prête à parcourir pour se procurer un bien ou un service et sur un seuil minimum de clientèle pour que perdure la fourniture de ce bien ou service. Ainsi, plus la ville est grande, plus ses fonctions urbaines sont affirmées, nombreuses, variées avec un plus grand niveau de rareté ; inversement, plus la ville est petite, plus ses fonctions sont réduites. La loi rang-taille insiste quant à elle sur le fait que le rang qu’occupe une ville dans un classement est en fonction de sa population. Ainsi, la première ville (rang 1) est la plus peuplée, les villes de rang 2 le sont moitié moins, etc. le rapport entre chaque rang serait universel dans le temps et l’espace. Sur un graphique à échelle logarithmique, les villes s’alignent alors selon une droite qui rend compte de la hiérarchie. Toute ville qui s’en écarte présente une anomalie qui correspond à une situation particulière.

